import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() =>
    runApp(
      Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: MaterialApp(
            home: Scaffold(
              backgroundColor: Colors.grey,
              appBar: AppBar(
                centerTitle: true,
                title: Text('Image Demo'),
                backgroundColor: Colors.blueGrey[1000],
              ),
              body: Center(
                child: Image(
                  image: AssetImage('images/diamond.jpg'),
                ),
              ),
            ),
          ),
        ),
      ),
    );


